#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "elf.h"

int main()
{
	int file,i;
	struct stat fileStat;
	Elf32_Shdr *shdr,*str_table;
    Elf32_Ehdr *endr;
    void* p;
    char* str;

	file=open("a.out",O_RDONLY);
	stat("a.out",&fileStat);
   	p=mmap (0,fileStat.st_size, PROT_READ | PROT_EXEC, MAP_PRIVATE, file, 0);
    endr=(Elf32_Ehdr *)p;
    shdr=(Elf32_Shdr*)((p)+(endr->e_shoff));
    str_table = &shdr[endr->e_shstrndx];
    str = ((p)+(str_table->sh_offset));
    printf("index name        \t    offset\tadress\t    size(bytes)\n");
    for(i=0;i<(endr->e_shnum);i++)
        printf("%-5d %-20s  %-10x  %-10x  %x\n",i,((str)+shdr[i].sh_name),shdr[i].sh_offset,shdr[i].sh_addr,shdr[i].sh_size);
    munmap(p,fileStat.st_size);
	return(0);
}
