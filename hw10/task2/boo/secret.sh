#!/bin/sh

func()
{
  compressed="$(echo $1 | sed -e 's/[^[:alnum:]]//g')"

  if [ "$compressed" != "$input" ] ; then
    return 1
  else
    return 0
  fi
}

# Sample usage of this function in a script

echo -n "Enter input: "
read input

if ! func "$input" ; then
  echo "boooooo."
  exit 1
else
  echo "good job!"
fi

exit 0
