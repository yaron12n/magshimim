#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PATH_MAX 2048

int main()
{
  pid_t child;
  int pipefd[2],curr_status;
  char dest[PATH_MAX];

  pipe(pipefd);
  child=fork();
  if (child==0)
  {
    write(pipefd[1], "magshimim", sizeof("magshimim"));
    exit(0);
  }
  else
  {
    waitpid(child,&curr_status,0);
    read(pipefd[0],dest,sizeof(dest));
  }
  if(!strcmp(dest,"magshimim"))
  {
    printf("Success!\n");
  }
  return(0);
}