#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include "LineParser.h"

#define PATH_MAX 2048
int execute(cmdLine* pCmdLine);
void Redirection(cmdLine* pCmdLine);
int main()
{

	char dest[PATH_MAX];
	cmdLine* command_info;
	pid_t child_pid;
	int i,curr_status,pipefd[2];
	while(1)
	{
		getcwd(dest,PATH_MAX);
		printf("%s$ ",dest);
		fgets(dest,PATH_MAX,stdin);
		command_info=parseCmdLines(dest);
		
		
		if(strcmp(command_info->arguments[0],"quit")==0)
		{
			freeCmdLines(command_info);	
			exit(0);
		}
		else if(!strcmp(command_info->arguments[0],"cd"))
		{
			chdir(command_info->arguments[1]);
			perror("cd");
		}
		else if(strcmp(command_info->arguments[0],"myecho")==0)
		{
			for(i=1;i<=(command_info->argCount) -1; i++)
				printf("%s ",command_info->arguments[i]);
			printf("\n");
		}
		else
		{
			if(strchr(arg , '|'))
			{
				pipe(fd);
				sonID = fork();
				if(sonID == 0)
				{	
					close(1);		
					dup(fd[1]);
					close(fd[1]);
					if(pCmdLine->inputRedirect != NULL || pCmdLine->outputRedirect != NULL)
					{
						Redirection(pCmdLine);
					}
					else
					{
						err = execvp(pCmdLine->arguments[0],pCmdLine->arguments);
					}
					if(err == -1)
					{
						perror("");
					}
				}
				else
				{
					close(fd[OUT]);
					sonID2 = fork();
				
					if(sonID2 == 0)
					{
						close(0);		
						dup(fd[IN]);
						close(fd[IN]);
						if(pCmdLine->next->inputRedirect != NULL || pCmdLine->next->outputRedirect != NULL)
						{
							Redirection(pCmdLine->next);
						}
						else
						{
							err = execvp(pCmdLine->next->arguments[0],pCmdLine->next->arguments);
						}
						if(err == -1)
						{
							perror("");
						}
			child_pid=fork();
			if(child_pid==0)
			{
				Redirection(command_info);
				exit(0);
			}
			if(command_info->blocking)
			{
				waitpid(child_pid, &curr_status, 0);
			}
			else
			{
				wait(&curr_status);
			}		
		}
		freeCmdLines(command_info);
	}
}

int execute(cmdLine* pCmdLine)
{
	execvp(pCmdLine->arguments[0],pCmdLine->arguments);
	perror("execvp");
	exit(0);
	
	return(0);
}
void Redirection(cmdLine* pCmdLine)
{
	int d1,d2;

	if((pCmdLine->outputRedirect) && (pCmdLine->inputRedirect))
	{
		close(1);
		close(0);
		d1 = open(pCmdLine->inputRedirect, S_IROTH);
		d2 = open(pCmdLine->outputRedirect, O_WRONLY);
		dup2(0,d1);
		dup2(1,d2);
		execute(pCmdLine);
	}
	else if(pCmdLine->inputRedirect)
	{
		close(0);
		d1 = open(pCmdLine->inputRedirect, S_IROTH);
		dup2(0,d1);
		execute(pCmdLine);
	}
	else if(pCmdLine->outputRedirect)
	{
		close(1);
		d1 = open(pCmdLine->outputRedirect, O_WRONLY);
		dup2(1,d1);
		execute(pCmdLine);
	}
	else
	{
		execute(pCmdLine);
	}
}