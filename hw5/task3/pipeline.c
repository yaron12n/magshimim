#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>


int main()
{
	int Pipe[2],stat1,stat2;
	pid_t child1,child2;
         char* str1[]={"ls","-l",NULL};
         char* str2[]={"tail","-n 2",NULL};

	pipe(Pipe);
	child1=fork();
	if(child1==0)
	{
		close(1);
		dup(Pipe[1]);
		close(Pipe[1]);
                if(execvp(str1[0],str1)) 
		{
			perror("!");
			return (0);
                }
	}
	else
	{
		close(Pipe[1]);
		child2=fork();
		if(child2==0)
		{
			close(0);
			dup(Pipe[0]);
			close(Pipe[0]);
        		if(execvp(str2[0],str2)) 
			{
				perror("?");
				return (0);
                 	}
		}
		else
		{
			close(Pipe[0]);
                	waitpid(child1 ,&stat1, 0);
                	waitpid(child2, &stat2, 0);
		}
	}
	return(0);
}
