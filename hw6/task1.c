#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
int main()
{
	pid_t child;
	close(1);
	open("output.txt",O_WRONLY | O_CREAT,S_IRWXU);
	child = fork();
	if(!child)
	{
		while(1)
			printf("I'm still alive\n");
		exit(0);
	}
	else
	{
		sleep(1);
		printf("Dispatching\n"); 
		if(kill(child,9)==-1)
			perror("kill");
		else
			printf("Dispached\n");
	}
	return 0;
}