#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>

void quit(int d1)
{
	char input[1024];

	printf("To quit or not to quit? that is the question. (y/n)\n");
	scanf("%s",input);
	if((input[0]=='y')|| (input[0]=='Y'))
	{
		close(d1);
		exit(0);
	}
}
int main()
{
	int i;
	int d1=open("output2.txt",O_WRONLY|O_CREAT);

    signal(2,quit);
    signal(3,quit);
    signal(15,quit);
    for(i=1;i<=2000000000;i++)
    {
    	if(i%1000000==0) 
    		printf("I'm still alive i = %d\n",i);
    }
    close(d1);
    exit(EXIT_SUCCESS);
}


