#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "LineParser.h"

#define PATH_MAX 2048
int execute(cmdLine* pCmdLine);

int main()
{

	char dest[PATH_MAX];
	cmdLine* command_info;
	pid_t child_pid;
	int i,curr_status;
	while(1)
	{
		getcwd(dest,PATH_MAX);
		printf("%s$ ",dest);
		fgets(dest,PATH_MAX,stdin);
		command_info=parseCmdLines(dest);
		
		
		if(strcmp(command_info->arguments[0],"quit")==0)
		{
			freeCmdLines(command_info);	
			exit(0);
		}
		else if((command_info->arguments[0][0] == 'c') && (command_info->arguments[0][1] == 'd'))
		{
			chdir(command_info->arguments[1]);
			perror("cd");
		}
		else if(strcmp(command_info->arguments[0],"myecho")==0)
		{
			for(i=1;i<=(command_info->argCount) -1; i++)
				printf("%s ",command_info->arguments[i]);
			printf("\n");
		}
		else
		{
			child_pid=fork();
			if(child_pid==0)
			{
			
				execute(command_info);
				exit(0);
			}
			if(command_info->blocking)
			{
				waitpid(child_pid, &curr_status, 0);
			}
			else
			{
				wait(&curr_status);
			}		
		}
		freeCmdLines(command_info);
	}
}

int execute(cmdLine* pCmdLine)
{
	execvp(pCmdLine->arguments[0],pCmdLine->arguments);
	perror("execvp");
	exit(0);
	
	return(0);
}
